//
//  main.swift
//  TextPushkin
//
//  Created by Private on 12/18/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

let initial: String = "Глухой глухого звал к суду судьи глухого. Глухой кричал моя им сведена корова - Помилуй - возопил глухой тому в ответ: \n \nСей пустошью владел еще покойный дед Судья решил: Чтоб не было разврата, Жените молодца, хоть девка виновата\n"

print(initial)

let first: String = "Глухой глухого звал к суду судьи глухого"
let second: String = "Глухой кричал моя им сведена корова"
let third: String = "Помилуй - возопил глухой тому в ответ"
let fourth: String = "Сей пустошью владел еще покойный дед"
let fifth: String = "Судья решил"
let sixth: String = "Чтоб не было разврата, жените молодца,хоть девка виновата"

var pushkin:[String] = []
pushkin.append(first)
pushkin.append(second)
pushkin.append(third)
pushkin.append(fourth)
pushkin.append(fifth)
pushkin.append(sixth)

print(pushkin)

var newPushkin: [String] = []

for value in pushkin {
    let needle = value.index(of: " ")
    let substr = value.prefix(upTo: needle!)
    newPushkin.append(String(substr))
}
print("\n\(newPushkin)")

var reversedNewPushkin: [String] = []

for value in newPushkin.reversed() {
    reversedNewPushkin.append(value)
}
print("\n\(reversedNewPushkin)")

extension String {
    public var upperLowerCasedCharacters: String {
        if characters.count > 1 {
            let splitIndex1 = index(after: startIndex)
            let splitIndex2 = index(before: endIndex)
            let firstCharacter = substring(to: splitIndex1).lowercased()
            let lastCharacter = substring(from: splitIndex2).uppercased()
            
            let sentence = self[splitIndex1..<splitIndex2]
            return firstCharacter + sentence + lastCharacter
        } else {
            return self
        }
    }
}

var devidedByWords: [String] = ["Глухой","глухого","звал","к","суду","судьи","глухого","Глухой","кричал","моя","им","сведена","корова","Помилуй","возопил","глухой","тому","в","ответ","Сей","пустошью","владел","еще","покойный", "дед","Судья","решил","Чтоб","не","было","разврата","Жените","молодца","хоть","девка","виновата"]

var originalPushkin : [String] = []

for value in devidedByWords {
    let upDown = value.upperLowerCasedCharacters
    originalPushkin.append(upDown)
    }
print("\n\(originalPushkin)")


